
# JUNEWAY Less1.2
# @smirkon

## Задачи по процессам
---

1. Опишите, что происходит, при выполнении любой команды в консоли, например:
    `$ ls -l` 

    ---

  - При запуске команды из bash с помощью функции `fork()` запускается новое адресное простарнство, полностью исходному, в котором функцией exec() запускается дочерний процесс, в котором выполняется указанная команда `ls -l`. Родительский процесс при этом с помощью функции wait() ожидает завершения дочернего процесса.
---

2. Используя ключ 'o', выведите с помощью `ps` список всех процессов в таком формате:
    `PID USER     %CPU    VSZ TT       COMMAND`
    пояснение: ID процесса, пользователь (real user), % загрузки CPU, размер виртуальной памяти, управляющий терминал, команда
    прикрепите вывод к задаче, перенаправив вывод на `head`:

    `$ ps ... ваши опции ... | head`

    ---

    ```bash
    $ ps ps ax -o pid,ruser=USER,%cpu,vsz,tt,comm | head
    PID USER     %CPU    VSZ TT       COMMAND
      1 root      0.0   3292 ?        init
      2 root      0.0      0 ?        kthreadd
      3 root      0.0      0 ?        rcu_gp
      4 root      0.0      0 ?        rcu_par_gp
      6 root      0.0      0 ?        kworker/0:0H-events_highpri
      8 root      0.0      0 ?        mm_percpu_wq
      9 root      0.0      0 ?        rcu_tasks_rude_
     10 root      0.0      0 ?        rcu_tasks_trace
     11 root      0.0      0 ?        ksoftirqd/0
    ```

---

3. Создайте процесс sleep с помощью 
	    `$ sleep infinity &`
	    
   - найдите его в списке процессов с помощью `ps`. 
   - отфильтруйте вывод `ps` чтобы получить строку только с этим процессом.
   - убейте процесс, отправив ему сигнал '-9' программой `kill`.
   - убедитесь, что процесса больше нет в списке выдачи `ps`.
   ---

    ```bash
    $ slesleep infinity &
    [1] 224130
    
    $ ps ps aux | grep sleep | grep -v grep
    s_admin   224130  0.0  0.0  74864   516 pts/0    S    18:07   0:00 sleep infinity
    
    $ pkipkill -9 sleep
    [1]+  Убито              sleep infinity
    
    $ ps ps aux | grep sleep | grep -v grep
    
    $ ps aux | grep sleep 
    s_admin   224293  0.0  0.0  75764   664 pts/0    S+   18:08   0:00 grep sleep
    ```