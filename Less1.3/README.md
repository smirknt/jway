
# JUNEWAY Less1.3
# @smirkon

## Задача по chroot
---

1. Заставить работать ls и cp
см Видео выше.
Вывод консоли в ответ.

    ---

  - Выполняем подготовительное копирование:
    ```bash
    s_admin@jway:~$ mkdir Less1.3 Less1.3/bin Less1.3/lib Less1.3/lib64
    
    s_admin@jway:~$ ldd /bin/bash
    	linux-vdso.so.1 (0x00007ffdd99c3000)
    	libtinfo.so.6 => /lib/x86_64-linux-gnu/libtinfo.so.6 (0x00007fbc2d5e3000)
    	libdl.so.2 => /lib/x86_64-linux-gnu/libdl.so.2 (0x00007fbc2d5dd000)
    	libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007fbc2d418000)
    	/lib64/ld-linux-x86-64.so.2 (0x00007fbc2d763000)
    
    s_admin@jway:~$ cp /lib/x86_64-linux-gnu/libtinfo.so.6 /lib/x86_64-linux-gnu/libdl.so.2 /lib/x86_64-linux-gnu/libc.so.6 Less1.3/lib
    
    s_admin@jway:~$ cp /lib64/ld-linux-x86-64.so.2 Less1.3/lib64
    
    s_admin@jway:~$ cp /bin/bash Less1.3/bin
    
    s_admin@jway:~$ ldd /bin/cp
    	linux-vdso.so.1 (0x00007ffc8f136000)
    	libselinux.so.1 => /lib/x86_64-linux-gnu/libselinux.so.1 (0x00007ff0ddc7c000)
    	libacl.so.1 => /lib/x86_64-linux-gnu/libacl.so.1 (0x00007ff0ddc71000)
    	libattr.so.1 => /lib/x86_64-linux-gnu/libattr.so.1 (0x00007ff0ddc69000)
    	libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007ff0ddaa4000)
    	libpcre2-8.so.0 => /lib/x86_64-linux-gnu/libpcre2-8.so.0 (0x00007ff0dda0c000)
    	libdl.so.2 => /lib/x86_64-linux-gnu/libdl.so.2 (0x00007ff0dda06000)
    	/lib64/ld-linux-x86-64.so.2 (0x00007ff0ddce6000)
    	libpthread.so.0 => /lib/x86_64-linux-gnu/libpthread.so.0 (0x00007ff0dd9e2000)
    
    s_admin@jway:~$ cp /lib/x86_64-linux-gnu/libselinux.so.1 /lib/x86_64-linux-gnu/libacl.so.1 /lib/x86_64-linux-gnu/libattr.so.1 /lib/x86_64-linux-gnu/libc.so.6 /lib/x86_64-linux-gnu/libpcre2-8.so.0 /lib/x86_64-linux-gnu/libdl.so.2 /lib/x86_64-linux-gnu/libpthread.so.0 Less1.3/lib
    
    s_admin@jway:~$ cp /lib64/ld-linux-x86-64.so.2 Less1.3/lib64
    
    s_admin@jway:~$ cp /bin/cp Less1.3/bin
    
    s_admin@jway:~$ ldd /bin/ls
    	linux-vdso.so.1 (0x00007ffcab535000)
    	libselinux.so.1 => /lib/x86_64-linux-gnu/libselinux.so.1 (0x00007f43a75ad000)
    	libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007f43a73e8000)
    	libpcre2-8.so.0 => /lib/x86_64-linux-gnu/libpcre2-8.so.0 (0x00007f43a7350000)
    	libdl.so.2 => /lib/x86_64-linux-gnu/libdl.so.2 (0x00007f43a734a000)
    	/lib64/ld-linux-x86-64.so.2 (0x00007f43a7617000)
    	libpthread.so.0 => /lib/x86_64-linux-gnu/libpthread.so.0 (0x00007f43a7328000)
    
    s_admin@jway:~$ cp /lib/x86_64-linux-gnu/libselinux.so.1 /lib/x86_64-linux-gnu/libc.so.6 /lib/x86_64-linux-gnu/libpcre2-8.so.0 /lib/x86_64-linux-gnu/libdl.so.2 /lib/x86_64-linux-gnu/libpthread.so.0 Less1.3/lib
    
    s_admin@jway:~$ cp /lib64/ld-linux-x86-64.so.2 Less1.3/lib64
    
    s_admin@jway:~$ cp /bin/ls Less1.3/bin
    ```
  - А теперь запускаем `chroot` на директорию `Less1.3` и проверяем работают ли `cp` и `ls`.
    ```bash
    s_admin@jway:~$ sudo chroot Less1.3
    
    I have no name!@/:~$ cp
    cp: missing file operand
    Try 'cp --help' for more information.
    
    I have no name!@/:~$ ls
    bin  lib  lib64

    ```